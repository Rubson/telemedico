<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Helper\LoginUser;

class DashboardController extends Controller {

    /** @var SessionInterface  */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @return Response
     */
    public function dashboard() {
        if(LoginUser::isLogged($this->session)) {
            return $this->render('index.html.twig');
        } else {
            return $this->redirect($this->generateUrl('login'));
        }
    }
}