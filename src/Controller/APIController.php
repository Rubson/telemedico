<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Helper\Authorization;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class APIController extends Controller {

    public function __construct(Authorization $authorization, RequestStack $requestStack) {
        $request =  $requestStack->getCurrentRequest();
        $givenToken = $request->headers->get('X-AUTH-TOKEN');
        if(!$authorization->authorize($givenToken)) {
            throw new AccessDeniedHttpException('Access Danied');
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse {
        $entityManager = $this->getDoctrine()->getManager();
        $date = json_decode($request->get('data'), true);
        $user = new User();
        $user->setUsername($date['username']);
        $user->setPassword($date['password']);
        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse(['response' => 'ok']);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function read($id): JsonResponse {
        $user = $this->getDoctrine()->getRepository(User::class)->findById($id);

        return new JsonResponse(json_encode($user));
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id): JsonResponse {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $date = json_decode($request->get('data'), true);
        $user->setUsername($date['username']);
        $user->setPassword($date['password']);
        $entityManager->persist($user);
        $entityManager->flush();
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return new JsonResponse(['response' => 'ok']);
    }
}