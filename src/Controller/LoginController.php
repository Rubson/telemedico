<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Helper\LoginUser;
use App\Form\LoginType;

class LoginController extends Controller {

    /** @var EntityManagerInterface  */
    private $em;
    /** @var SessionInterface  */
    private $session;

    public function __construct(EntityManagerInterface $em, SessionInterface $session)
    {
        $this->em = $em;
        $this->session = $session;
    }

    public function form(Request $request) {
        $user = new User();
        $repository = $this->em->getRepository(User::class);
        $form = $this->createForm(LoginType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $credentials = $repository->findOneByLoginAndPassword($user->getUsername(), $user->getPassword());
            if($credentials) {
                LoginUser::login($user->getUsername(), $this->session);

                return $this->redirect($this->generateUrl('dashboard'));
            }
        } else {
            return $this->render('login/form.html.twig', array(
                'form' => $form->createView()
            ));
        }
    }
}