<?php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * @param string $login
     * @param string $password
     * @return int
     */
    public function findOneByLoginAndPassword(string $login, string $password): int
    {
        $qb = $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->where('u.username = :username')
            ->andWhere('u.password = :password')
            ->setParameter('username', $login)
            ->setParameter('password', $password);
        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function findById(int $id): ?array {
         $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.id = :id')
            ->setParameter('id', $id);
        $query = $qb->getQuery();

        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}