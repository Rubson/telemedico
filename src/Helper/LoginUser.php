<?php
namespace App\Helper;

use Symfony\Component\HttpFoundation\Session\Session;

class LoginUser
{
    /**
     * @param $login
     * @param Session $session
     */
    public static function login($login, Session $session) {
        $session->start();
        $session->set('user', $login);
    }

    /**
     * @param Session $session
     * @return bool
     */
    public function isLogged(Session $session): bool {
        return  $session->get('user');
    }
}