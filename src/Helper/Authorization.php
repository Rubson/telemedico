<?php
namespace App\Helper;

class Authorization
{
    private $apikey;

    public function __construct(string $apiKey = 'aaa')
    {
        $this->apikey = $apiKey;
    }

    /**
     * @param $key
     * @return bool
     */
    public function authorize($key): bool {
        return $this->apikey === $key;
    }
}